export class Project {
  name: string;
  company: string;
  description: string;
  timeline: string;
  imageURL: string;
  websiteURL?: string;
  tags: string[];
}
